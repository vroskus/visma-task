'use strict';

const clear = require('clear');
const figlet = require('figlet');
const chalk = require('chalk');
const touch = require('touch');
const _ = require('lodash');

const csv = require('fast-csv');
const fs = require('fs'); 
const validator = require('email-validator');
const inquirer = require('inquirer');

const db = './db';

const init = () => {
    touch(db);
    showDisclaimer();
    askForCommand();
};

const showDisclaimer = async () => {
    clear();
    console.log(
        chalk.yellow(
            figlet.textSync('Staff Register', { horizontalLayout: 'full' })
        )
    );
};

const addRecord = async (db, record) => {
    const lastId = await validateRecord(db, record.firstname, record.lastname, record.email);

    return new Promise (resolve => {
        if (lastId !== null) {
            const preparedRecord = _.values(record);
            preparedRecord.unshift(lastId);

            csv
                .writeToStream(fs.createWriteStream(db, { flags: 'a' }), [preparedRecord], { includeEndRowDelimiter: true })
                .on('finish', () => {
                    resolve(true);
                });
        } else {
            resolve(false);
        }
    });
};

const validateRecord = async (db, firstname, lastname, email) => {
    let lastId = 1;

    return new Promise(resolve => {
        csv
            .fromPath(db)
            .on('data', data => {
                if (firstname === data[1] && lastname === data[2] && email === data[3]) {
                    resolve(null);
                }
                lastId = ++data[0];
            })
            .on('end', () => {
                resolve(lastId);
            });
    });
};

const findRecords = async (db, phrase) => {
    const foundRecords = [];

    return new Promise(resolve => {
        csv
            .fromPath(db)
            .on('data', data => {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].indexOf(phrase) !== -1) {
                        foundRecords.push(data);
                        break; // Stop if found
                    }
                }
                
            })
            .on('end', () => {
                resolve(foundRecords);
            });
    });
}

const deleteRecord = async (db, id) => {
    const records = [];
    let recordFound = false;

    return new Promise(resolve => {
        csv
            .fromPath(db)
            .on('data', data => {
                if (data[0] !== id) {
                    records.push(data);
                } else {
                    recordFound = true;
                }
            })
            .on('end', () => {
                if (!recordFound) {
                    resolve(false) 
                }

                csv
                    .writeToStream(fs.createWriteStream(db), records, { includeEndRowDelimiter: true })
                    .on('finish', () => {
                        resolve(true);
                    });
            });
    });
};

const importRecords = async (db, filePath) => {
    if (!fs.existsSync(filePath)) {
        return false;
    }

    return new Promise(resolve => {
        const csvstream = csv
            .fromPath(filePath, { delimiter: ';' })
            .on('data', async (data) => {
                if (data.length === 6 && data[0].length !== 0 && data[1].length !== 0 && validator.validate(data[2])) {
                    let record = {
                        firstname: data[0],
                        lastname: data[1],
                        email: data[2],
                        phonenumber1: data[3],
                        phonenumber2: data[4],
                        comment: data[5],
                    };
                    csvstream.pause();
                    await addRecord(db, record);
                    csvstream.resume();
                }
            })
            .on('end', () => {
                resolve(true);
            });
    });
};

const askForCommand = async () => {
    const attrs = [
        {
            name: 'command',
            type: 'input',
            message: 'Enter command:',
            validate: (value) => {
                return value.length !== 0;
            }
        }
    ];

    const result = await inquirer.prompt(attrs);

    const commandFragments = result.command.split(' ');
    const command = commandFragments[0];
    const arg = commandFragments.length === 2 ? commandFragments[1] : null;
       
    if (_.has(availableCommands, command)) {
        if (availableCommands[command].arg) {
            if (arg !== null) {
                await availableCommands[command].command(arg);
            } else {
                console.log(chalk.red('Command arguments not provided!'));
            }
        } else {
            await availableCommands[command].command();
        }
    } else {
        console.log(chalk.red('Command not found!'));
        await commandHelp();
    }

    askForCommand();
};

const commandAdd = async () => {
    const attrs = [
        {
            name: 'firstname',
            type: 'input',
            message: 'Firstname:',
            validate: (value) => {
                return value.length !== 0 ? true : 'This field is required!';
            }
        },
        {
            name: 'lastname',
            type: 'input',
            message: 'Lastname:',
            validate: (value) => {
                return value.length !== 0 ? true : 'This field is required!';
            }
        },
        {
            name: 'email',
            type: 'input',
            message: 'Email:',
            validate: (value) => {
                switch (true) {
                    case value.length === 0:
                        return 'This field is required!';
                    case !validator.validate(value):
                        return 'Email is not valid';
                    case validator.validate(value):
                        return true;
                    default:
                        return false;
                }
            }
        },
        {
            name: 'phonenumber1',
            type: 'input',
            message: 'Primary phone number:'
        },
        {
            name: 'phonenumber2',
            type: 'input',
            message: 'Secondary phone number:'
        },
        {
            name: 'comment',
            type: 'input',
            message: 'Comment:'
        }
    ];

    const record = await inquirer.prompt(attrs);

    try {
        const result = await addRecord(db, record);
        if (result) {
            console.log(chalk.green('Record added!'));
        } else {
            console.log(chalk.red('Unable to add record!'));
        }
    } catch (err) {
        console.log(chalk.red(err));
    }
};

const commandImport = async (filePath) => {
    try {
        const result = await importRecords(db, filePath);
        if (result) {
            console.log(chalk.green('Records imported!'));
        } else {
            console.log(chalk.red('Unable to import records!'));
        }
    } catch (err) {
        console.log(chalk.red(err));
    }
};

const commandFind = async (phrase) => {
    try {
        const result = await findRecords(db, phrase);
        if (result.length !== 0) {
            for (let i = 0; i < result.length; i++) {
                console.log(result[i].join(' | '));
            }
            console.log(chalk.green(`${result.length} records found!`));
        } else {
            console.log(chalk.yellow('No records found!'));
        }
    } catch (err) {
        console.log(chalk.red(err));
    }
};

const commandDelete = async (id) => {
    try {
        const result = await deleteRecord(db, id);
        if (result) {
            console.log(chalk.green('Record deleted!'));
        } else {
            console.log(chalk.red('Unable to delete record!'));
        }
    } catch (err) {
        console.log(chalk.red(err));
    }
};

const commandHelp = async () => {
    console.log(chalk.green('List of available commands:'));

    for (var key in availableCommands) {
        console.log(`- ${key} ${availableCommands[key].comment}`);
    }
};

const availableCommands = {
    'add': {
        comment: ': Adds a new record',
        arg: false,
        command: commandAdd,
    },
    'import': {
        comment: '<file_path> : Import a CSV file of records',
        arg: true,
        command: commandImport,
    },
    'find': {
        comment: '<search_phrase> : Finds a record by a given search phrase',
        arg: true,
        command: commandFind,
    },
    'delete': {
        comment: '<ID> : Deletes record from the database by a given ID',
        arg: true,
        command: commandDelete,
    },
    'help': {
        comment: ': Returns this help',
        arg: false,
        command: commandHelp,
    },
    'exit': {
        comment: ': Closes the application',
        arg: false,
        command: process.exit,
    },
};

module.exports = {
    init,
    addRecord,
    validateRecord,
    findRecords,
    deleteRecord,
    importRecords
};

require('make-runnable/custom')({
    printOutputFrame: false
})