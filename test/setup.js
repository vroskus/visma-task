const touch = require('touch');
const fs = require('fs');

const db = './test/test_db';
const sample_data = '1,firstname,lastname,email,phonenumber1,phonenumber2,comment\r\n';

const up = () => {
    return new Promise(resolve => {
        touch(db);
        fs.appendFile(db, sample_data, err => {
            resolve(true);
            if (err) {
                return console.log(err);
            }
        });
    });
};

const down = () => {
    return new Promise(resolve => {
        fs.unlink(db, err => {
            resolve(true);
            if (err) {
                return console.log(err);
            }
        });
    });
};

module.exports = {
    db,
    up,
    down,
};