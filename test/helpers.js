const csv = require('fast-csv');

const countDbRecords = (db) => {
    let recordsQuantity = 0;

    return new Promise(resolve => {
        csv
            .fromPath(db)
            .on('data', data => {
                if (data.length !== 0) {
                    recordsQuantity++;
                }
            })
            .on('end', () => {
                resolve(recordsQuantity);
            });
    });
};

module.exports = {
    countDbRecords,
};