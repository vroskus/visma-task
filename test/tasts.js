const expect = require('chai').expect;
const app = require('../app');
const setup = require('./setup');
const helpers = require('./helpers');

describe('validateRecord()', () => {

    before(() => setup.up());

    it('should fail validation and find duplicate', async () => {
        const result = await app.validateRecord(setup.db, 'firstname', 'lastname', 'email');
        expect(result).to.be.null;
    });

    it('should pass validation and get next record ID', async () => {
        const result = await app.validateRecord(setup.db, 'firstname_mod', 'lastname', 'email');
        expect(result).to.equal(2);
    });

    after(() => setup.down());

});

describe('addRecord()', () => {

    before(() => setup.up());

    const record = {
        firstname: 'firstname_mod',
        lastname: 'lastname',
        email: 'email',
        phonenumber1: 'phonenumber1',
        phonenumber2: 'phonenumber2',
        comment: 'comment',
    };

    it('should add record', async () => {
        const result = await app.addRecord(setup.db, record);
        expect(result).to.be.true;
        const recordsQuantity = await helpers.countDbRecords(setup.db);
        expect(recordsQuantity).to.equal(2);
    });

    it('should not add record', async () => {
        const result = await app.addRecord(setup.db, record);
        expect(result).to.be.false;
        const recordsQuantity = await helpers.countDbRecords(setup.db);
        expect(recordsQuantity).to.equal(2);
    });

    after(() => setup.down());

});

describe('findRecords()', () => {

    before(() => setup.up());

    it('should find record', async () => {
        const result = await app.findRecords(setup.db, 'firstname_mod');
        expect(result).to.be.an('array').that.is.empty;
    });

    it('should not find record', async () => {
        const result = await app.findRecords(setup.db, 'firstname');
        expect(result).to.be.an('array').that.is.not.empty;
        expect(result).to.have.lengthOf(1);
    });

    after(() => setup.down());

});

describe('deleteRecord()', () => {

    before(() => setup.up());

    it('should not delete record', async () => {
        const result = await app.deleteRecord(setup.db, '2');
        expect(result).to.be.false;
    });

    it('should delete record', async () => {
        const result = await app.deleteRecord(setup.db, '1');
        expect(result).to.be.true;
    });

    after(() => setup.down());

});