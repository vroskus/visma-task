# Staff Register

Visma task

Task description can be found in `./CLI test assignment - frameworkless.docx`

## System requirements

 - node.js
 - npm
 - git

## Installation

First you have to clone remote project repository
```sh
$ git clone https://bitbucket.org/vroskus/visma-task.git task
```

Enter project directory
```sh
$ cd task
```

Install all dependencies
```sh
$ npm install
```

## Configuration

There is no such

## Running the application

```sh
$ npm run-script start
```

## Running tests

There are several example tests provided

To run tests
```sh
$ npm run-script test
```

## Adding sample data

There are several predefined records that can be imported

To run tests
```sh
$ import ./sample_data/file.csv
```